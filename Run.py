#!/usr/bin/python3

import sys
import Server.Main
import Client.Main

if len(sys.argv) > 1:
    if sys.argv[1].lower() == 'server':
        Server.Main.run()
    elif sys.argv[1].lower() == 'client':
        if len(sys.argv) > 2:
            Client.Main.run(sys.argv[2])
        else: Client.Main.run('')