#!/usr/bin/python3

import socket
import pickle
import Constants

class Network:
    def __init__(self, ip, port, kind, override_socket = None):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.ip = ip
        self.port = port
        self.addr = (self.ip, self.port)
        self.init_data = None
        if kind == 'server':
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
            self.bind()
        elif kind == 'client':
            if override_socket:
                self.socket = override_socket
            else:
                self.init_data = self.connect()

    def bind(self):
        try:
            self.socket.bind(self.addr)
            self.socket.listen(5)
        except socket.error as e:
            print(e)

    def connect(self):
        try:
            self.socket.connect(self.addr)
            return pickle.loads(self.socket.recv(Constants.BYTES))
        except:
            pass

    def exchange(self, data):
        try:
            self.socket.send(pickle.dumps(data))
            return pickle.loads(self.socket.recv(Constants.BYTES))
        except socket.error as e:
            print(e)
            return e

    def send(self, data):
        try:
            self.socket.send(pickle.dumps(data))
        except socket.error as e:
            print(e)
    
    def recv(self):
        try:
            return pickle.loads(self.socket.recv(Constants.BYTES))
        except socket.error as e:
            print(e)
            return e
    
    def accept(self):
        client_socket, client_addr = self.socket.accept()
        return Network(*client_addr, 'client', client_socket)

    def ack(self):
        self.socket.send(Constants.BYTES.to_bytes(Constants.BYTES, 'little'))
        self.socket.recv(Constants.BYTES)