#!/usr/bin/python3

import Game
import Constants
from Network import Network

import _thread

def run():
    print('server starting...')
    net = Network('', 9999, 'server')
    print('server started')

    id = 0
    game = Game.Game(Constants.BOARD_SIZE, Constants.START)
    while True:
        client_net = net.accept()
        print('connected to %s' % '%s:%d' % client_net.addr)
        id += 1
        
        if game.check_game_end(): 
            game = Game.Game(Constants.BOARD_SIZE, Constants.START)
        _thread.start_new_thread(client, (client_net, game, id))

def client(client_net:Network, game:Game.Game, id):

    client_net.send(game)

    while not game.check_game_end():
        key = client_net.recv()

        if key == ord('q'):
            break
            
        if key in Constants.KEYS: 
            game.dir = Constants.DIR_DICT[key]
            # print(game.dir)

        # if game.snake.grow():
        #     print(str(game.food.coords))
        client_net.send(game.food.coords)
        
        game.move()
        
        # pickle.loads(clientsocket.recv(Constants.BYTES))
        # clientsocket.send(pickle.dumps(game.move()))

if __name__ == '__main__':
    run()