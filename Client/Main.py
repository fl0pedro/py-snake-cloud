#!/usr/bin/python3

from Game import Game
from Network import Network
from Client.Display import Display
import Constants


def run(addr):
    net = Network(addr, 9999, 'client')
    game:Game = net.init_data
    if game.check_game_end(): return
    display = Display(Constants.BOARD_SIZE, Constants.DELAY)

    snake, _ = game.get_board()

    pause = False

    while not game.check_game_end():
        key = display.key_pressed()
        food = net.exchange(key)
        if key == ord('q'):
            break
        elif key == ord('p'):
            if pause: pause = False
            else: pause = True
        if pause: continue
            
        if key in Constants.KEYS:
            game.dir = Constants.DIR_DICT[key]

        game.move()
        
        game.sync_food(food)

        display.draw(snake.queue, game.food.coords)
        if Constants.SIZE > 4:
            display.scores(
                    game.points, 
                    game.frames, 
                    game.turns
        )
        # if not connected: display.alert('~disconnected!~')

    display.end()
    
    # print('victory: ', game.victory)
    print('points: ', game.points)
    print('frames: ', game.frames)
    print('turns: ', game.turns)

if __name__ == '__main__':
    run()