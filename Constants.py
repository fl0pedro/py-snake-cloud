#!/usr/bin/python3

from curses import KEY_RIGHT, KEY_LEFT, KEY_DOWN, KEY_UP

START = (7, 7)
SIZE = 16
BOARD_SIZE = (SIZE, SIZE)
DELAY = 0.1

DIR_DICT = {
        KEY_UP    : 'n',
        KEY_RIGHT : 'e',
        KEY_DOWN  : 's',
        KEY_LEFT  : 'w'
}

KEYS = list(DIR_DICT.keys())

BYTES = 2048